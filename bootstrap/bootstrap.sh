# Install Packages
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y make libesmtp-dev php5-dev php5-cli dovecot-postfix

# Generate extesion.
cd /vagrant
make clean
phpize --clean
phpize
./configure --with-authmail
make
sudo make install
#ldd modules/authmail.so;
sudo echo 'extension=authmail.so' > /etc/php5/cli/conf.d/authmail.ini

# config local smtp server.
sudo cp -f /vagrant/bootstrap/etc/main.cf /vagrant/bootstrap/etc/master.cf /etc/postfix
sudo cp -rf /vagrant/bootstrap/etc/conf.d /etc/dovecot

# restart server
sudo service dovecot restart
sudo service postfix restart

# Create authmail test user.
sudo useradd authmail -p '$6$idG04c5b$TQ17/EHvNdoHZAsJru8k324DE06kwQa9Bu7FhHxS6VNQr46gj6mYmLGX6Hk4rlOsHp45HHjU7o3hPpLoVEKst0'

# Send an test email.
php -c /vagrant/bootstrap/etc/php.ini /vagrant/bootstrap/test.php

