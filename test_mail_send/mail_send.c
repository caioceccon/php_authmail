#include "libesmtp.h"
#include <auth-client.h>
#include <stdio.h>
#include <stdlib.h>

struct authinfo_t {

    char *ai_user;
    char *ai_pass;
    char *ai_domain;
};

int authinteract (auth_client_request_t request, char **result,
int fields, void *arg)
{   
    
    struct authinfo_t *authinfo = (struct authinfo_t *)arg;
    int i;
    if(!authinfo)
       return 0;
        
    for (i = 0; i < fields; i++)
       {   
        if (request[i].flags & AUTH_USER)
          result[i] = authinfo->ai_user;
        else if (request[i].flags & AUTH_PASS)
          result[i] = authinfo->ai_pass;
        else if (request[i].flags & AUTH_REALM)
          /* Just supply an empty string for the domain */
          result[i] = authinfo->ai_domain;
        else
        return 0;
    }   
    return 1;
}   

int
main() {
    char *login   = "xxxx=digirati.com.br";
    char *pass    = "xxxxxxxxx";
    char *domain  = "digirati.com.br";
    char *to      = "xxxxxxxxxxxxx";
    char *from    = "xxxxxxxxxxxxx";
    char *message = "TESTE\r\n";
    char *subject = "subject\r\n";
    char *smtp    = "smtp.digirati.com.br:25";
    struct authinfo_t ai;
    
    smtp_session_t mysession;
    smtp_message_t mymessage;
    smtp_status_t mysmtpstatus;
    smtp_recipient_t myrecipient;
    auth_context_t authctx;


    printf("criando seesão \n ");
    auth_client_init ();
    mysession = smtp_create_session();
    mymessage = smtp_add_message(mysession);
    smtp_set_server(mysession, smtp);
  
    printf("setting authinfo \n "); 
    ai.ai_user   = login;
    ai.ai_pass   = pass;
    ai.ai_domain = domain;
  
    smtp_set_reverse_path (mymessage, from);
    
    smtp_set_header (mymessage, "From", NULL, from);
    smtp_set_header (mymessage, "To", NULL, to);
    smtp_set_header (mymessage, "Reply-To", NULL, "None");
    smtp_set_header (mymessage, "Subject", subject);
    smtp_set_header_option (mymessage, "Subject", Hdr_OVERRIDE, 1);

  
    myrecipient = smtp_add_recipient (mymessage, to);
  
    smtp_set_message_str(mymessage, (char*) message);
  
  
    smtp_add_recipient (mymessage, to);
    
    printf("criando auth sesstion \n"); 
    authctx = auth_create_context ();
    printf("Authctx criada \n ");

    printf("chamando auth_set_mechanism_flags \n ");
    auth_set_mechanism_flags (authctx, AUTH_PLUGIN_PLAIN, 0);
    smtp_auth_set_context (mysession, authctx);

    printf("chamando auth_set_interact_cb \n ");
    auth_set_interact_cb (authctx, &authinteract, &ai);



    if (!smtp_start_session (mysession)) {
          char buf[128];
    
          fprintf (stderr, "SMTP server problem %s\n",
                   smtp_strerror (smtp_errno (), buf, sizeof buf));
          return 42;
    }
      else
    {
          /* Report on the success or otherwise of the mail transfer.*/
    }
    
    /* Free resources consumed by the program.*/
    smtp_destroy_session (mysession);
    auth_destroy_context (authctx);
    auth_client_exit ();
    return 0;
}

