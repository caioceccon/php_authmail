PHP_ARG_WITH(authmail, whether to enable Auth Mail support,
[ --with-authmail[=DIR]  Enable Auth Mail support. DIR is an optional path to libesmtp prefix])

if test "$PHP_AUTHMAIL" != "no"; then

  AC_DEFINE(HAVE_AUTHMAIL,1,[Whether you have Auth Mail])

  AC_MSG_CHECKING(for libesmtp installation)

  if test "x$PHP_AUTHMAIL" != "xno" -a "x$PHP_AUTHMAIL" != "xyes"; then
    if test -f "$PHP_AUTHMAIL/include/libesmtp.h"; then
        PHP_AUTHMAIL_PREFIX="$PHP_AUTHMAIL"
    fi
  else
    for i in /usr /usr/local /opt; do
        if test -f "$i/include/libesmtp.h"; then
            PHP_AUTHMAIL_PREFIX="$i" && break
        fi
    done
  fi

  if test -z $PHP_AUTHMAIL_PREFIX; then
    AC_MSG_ERROR(not found)
  else
    AC_MSG_RESULT(found in $PHP_AUTHMAIL_PREFIX)
  fi

  PHP_ADD_LIBRARY_WITH_PATH(esmtp, $PHP_AUTHMAIL_PREFIX/$PHP_LIBDIR, AUTHMAIL_SHARED_LIBADD)

  PHP_SUBST(AUTHMAIL_SHARED_LIBADD)
  PHP_NEW_EXTENSION(authmail, authmail.c, $ext_shared)
fi
