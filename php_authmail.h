#ifndef PHP_AUTHMAIL_H
#define PHP_AUTHMAIL_H 1

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_BEGIN_MODULE_GLOBALS(authmail)
    long counter;
    zend_bool direction;
ZEND_END_MODULE_GLOBALS(authmail)

#ifdef ZTS
#define AUTHMAIL_G(v) TSRMG(authmail_globals_id, zend_authmail_globals *, v)
#else
#define AUTHMAIL_G(v) (authmail_globals.v)
#endif

#define PHP_AUTHMAIL_VERSION "1.0"
#define PHP_AUTHMAIL_EXTNAME "authmail"

PHP_MINIT_FUNCTION(authmail);
PHP_MSHUTDOWN_FUNCTION(authmail);
PHP_RINIT_FUNCTION(authmail);
PHP_MINFO_FUNCTION(authmail);
PHP_FUNCTION(amail);

extern zend_module_entry authmail_module_entry;
#define phpext_authmail_ptr &authmail_module_entry

#endif
