# php_authmail

This extension was made in order to provide authentication to the php mail funcitons, setting the authentication settings on a htaccess or any ini file making possible to use the same mail function with authentication without changing the funciton call code.

## Usage

### Htaccess Settings

The example bellow show a tipical htaccess file configuration with all configuration variables:

	  php_flag  authmail.debug ON
	  php_flag  authmail.default_port OFF
	  php_value authmail.address account@domain.com
	  php_value authmail.smtp smtp.domain.com:587
	  php_value authmail.smtp_account account=domain.com
	  php_value authmail.password "accountpassword"

#### authmail.debug

* This variable/flag is responsible for controlling the debug mode, when it is set to ON, any error caused by settings or the smtp server will be reported using the php warning, by default this variable is set to Off it means that if you don't set it to On any fail will pass silently, but the funciton will still return false in cause of any error.

#### authmail.default_port

* This variable/flag set to ON make the server door 25 be used to the smtp server settings.

#### authmail.address

* This variables set the email sender address used in the from header.

#### authmail.smtp

* This variable is used for setting the smtp server address, it also can be used to define the smtp server port, it can be used following pattern ADDRESS:DOOR as in the example smtp.domain.com:587, in the case that the door is omited the authmail.default_port will be used.

#### authmail.smtp_account

* This variable is used to set the smpt account used in the authentication processes in the smtp server.

#### authmail.password

* This variable set the smtp user password the variable should be set using single quotes to avoid problems with blank spaces and special characters.

#### Automatic settings discovery

From the 6 variables only 2 are required the other can be inferred based on this 2 variables assuming that server settings follow the standard below:

The smtp server is equal: ***smtp.\<domain>*** and
the smtp account is equal: ***\<user>=\<domain>***

Example htaccess:

	  php_value authmail.address myuser@mydomain.com
	  php_value authmail.password "mypassword"

Based in this 2 variables the extension will try to guess the other settings assuming that the following standard the smtp server is ***smtp.mydomain.com***  and the smtp user is ***myuser=mydomain.com***.

It means that the only required variables are ***authmail.address*** and ***authmail.password*** based in this two variables the extension will try to guess the other configurations.

## Installation
For generating debian package and installation instructions look at the bootstrap/bootstrap.sh which is the filed used by Vagrant machine to create the development environment.