#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <auth-client.h>
#include <setjmp.h>
#include <signal.h>
#include "php.h"
#include "php_ini.h"
#include "php_authmail.h"
#include "libesmtp.h"

#define SMTP_PORT_STR "25"
#define SUBMISSION_PORT_STR "587"


typedef struct authinfo {
    char *ai_user;
    char *ai_pass;
    char *ai_domain;
} authinfo_t;

typedef struct monitorinfo {
    int     mi_debug;
    jmp_buf mi_jmpbuf;
} monitorinfo_t;

static void
notify(char *message_error)
{
    char *slash, *executed_file;

    executed_file = estrdup(zend_get_executed_filename(TSRMLS_C));
    php_printf("%s %s.. ", executed_file, message_error);
    slash = strrchr(executed_file, '/');
    if (slash == NULL) {
        php_printf("Unexpected script path: %s", executed_file);
    } else {
        slash++;
        *slash = '\0';
        php_printf("Please configure it correctly in %s.htaccess.",
                   executed_file);
    }
    efree(executed_file);
}

static int
authinteract(auth_client_request_t request, char **result,
             int fields, void *arg)
{
    int i;
    authinfo_t *authinfo = (authinfo_t *)arg;

    if(!authinfo)
        return 0;

    for (i = 0; i < fields; i++) {
        if (request[i].flags & AUTH_USER)
            result[i] = authinfo->ai_user;
        else if (request[i].flags & AUTH_PASS)
            result[i] = authinfo->ai_pass;
        else if (request[i].flags & AUTH_REALM)
            result[i] = authinfo->ai_domain;
        else
            return 0;
    }
    return 1;
}

static void
monitor_cb(const char *buf, int buflen, int writing, void *arg)
{
    monitorinfo_t *monitorinfo = (monitorinfo_t *)arg;
    /* Writing equal SMTP_CB_READING means it is the server reply we don't want
     * to read what the program write in the socket it could cause wrong
     * behaviour like in the cases the strncmp found wrongly the string 535 or
     * 530 in the message. */
    if (writing == SMTP_CB_READING) {
        /* If the server reply 535 or 530 authentication fail or authentication
         * required in both cases it means there is wrong user name or password
         * then the program will jump to the end, clean all the resource and
         * return an error code */
        if (strncmp(buf, "535 ", 4) == 0 || strncmp(buf, "530 ", 4) == 0) {
            if (monitorinfo->mi_debug)
                notify("Error: authentication failed:"
                       " Wrong user name or password.");
            longjmp(monitorinfo->mi_jmpbuf, 2);
        }
        /* In case of the sender IP address to be blacklisted or the smtp
         * account be unavailable the smtp server will reply one of the
         * following codes 421, 451, 554, 553, 550 */
        /*if (monitorinfo->mi_debug) {
            if (strncmp(buf, "421 ", 4) == 0 ||
                strncmp(buf, "451 ", 4) == 0 ||
                strncmp(buf, "501 ", 4) == 0 ||
                strncmp(buf, "554 ", 4) == 0 ||
                strncmp(buf, "553 ", 4) == 0 ||
                strncmp(buf, "550 ", 4) == 0)
                    php_printf("%s ", buf);
        }*/
    }
}

static void
print_recipient_status(smtp_recipient_t recipient, const char *mailbox,
                       void *arg)
{
    const smtp_status_t *status;

    status = smtp_recipient_status(recipient);
    php_printf("%s: %d %s\n", mailbox, status->code, status->text);
}

static void
add_recipients(smtp_message_t message, char *content)
{
    char *recipient, *saveptr = NULL;
    /* Splitting the header content with "," and add each occurrence
     * as a message recipient. */
    for (recipient = strtok_r(content, ",", &saveptr); recipient;
        recipient  = strtok_r(NULL, ",", &saveptr))
        smtp_add_recipient(message, recipient);
}

static int
set_server(smtp_session_t session, char *smtp_address,
           char *sender_domain, char *port)
{
    char *smtp_server, *smtp_server_port, *smtp_address_port;
    int smtp_address_len, port_len, smtp_server_size;

    port_len = strlen(port);
    smtp_address_len = strlen(smtp_address);
    smtp_address_port  = strchr(smtp_address, ':');

    if (port_len == 0 && smtp_address_port == NULL && smtp_address_len > 0) {
        smtp_set_server(session, smtp_address);
        return 1;
    }

    if (smtp_address_port != NULL) {
        *smtp_address_port = '\0';
        smtp_address_port += sizeof(char);
    }

    if (port_len > 0) {
        smtp_server_port = port;

        if (smtp_address_port != NULL &&
            strlen(smtp_address_port) > 1 &&
            strcmp(smtp_address_port, port) != 0)
                notify("Warning: Port has been defined twice");
    } else {
        if (smtp_address_port != NULL) {
            smtp_server_port = smtp_address_port;
        } else {
            smtp_server_port = SUBMISSION_PORT_STR;
        }
    }

    if (smtp_address_len == 0) {
        smtp_server_size = strlen(sender_domain)
                         + strlen(smtp_server_port) + 7;/*smtp.:\0*/
        smtp_server      = emalloc(smtp_server_size);
        snprintf(smtp_server, smtp_server_size, "smtp.%s:%s",
                 sender_domain, smtp_server_port);
    } else {
        smtp_server_size = smtp_address_len + port_len
                         + strlen(smtp_server_port) + 2;/*:\0*/
        smtp_server      = emalloc(smtp_server_size);
        snprintf(smtp_server, smtp_server_size, "%s:%s",
                 smtp_address, smtp_server_port);
    }

    smtp_server_port  = strchr(smtp_server, ':');
    if (strlen(smtp_server_port) > 0)
        smtp_server_port += sizeof(char);

    if (strcmp(smtp_server_port, SUBMISSION_PORT_STR) != 0 &&
        strcmp(smtp_server_port, SMTP_PORT_STR) != 0) {
        notify("Error: You have not specified a valid port,"
               " allowed values are 25 or 587");
        efree(smtp_server);
        return 0;
    }
    smtp_set_server(session, smtp_server);
    efree(smtp_server);
    return 1;
}

static int
set_headers(smtp_message_t message, char *headers,
            int headers_len, char **invalid_headers)
{
    char *mail_headers, *p_header, *colon, *content, *tmp_header;
    char *inval_headers, *newptr, *content_cr_lf, *cr_lf;
    int inval_headers_size, extra_len, header_len;
    int cr_lf_size, content_len, inval_headers_len;

    extra_len          = (headers_len / 78) + 10;
    inval_headers_size = extra_len + headers_len + 1;
    inval_headers      = ecalloc(inval_headers_size, sizeof(char));
    inval_headers_len  = 0;
    cr_lf_size         = 3;
    cr_lf              = "\r\n";

    mail_headers = estrdup(headers);
    for (p_header = strtok_r(mail_headers, "\n", &tmp_header); p_header;
         p_header = strtok_r(NULL, "\n", &tmp_header)) {

        colon = strchr(p_header, ':');
        if (colon == NULL) {
            if (INI_INT("authmail.debug"))
                php_printf("Warning: The header option \"%s\""
                           " has been ignored! ", p_header);
            continue;
        }

        /* The header is splited as Title and Content as the example below
         * MIME-Version: 1.0 will be:
         * Title:MIME-Version
         * content:1.0 */
        *colon =  '\0';
        content = colon;
        content += sizeof(char);

        /* Removing blank spaces from the beginning. */
        while (isspace(*content))
            content += sizeof(char);

        /* Removing blank spaces from the end. */
        content_cr_lf = content;
        content_len = strlen(content_cr_lf);
        content_cr_lf = content_cr_lf + sizeof(char) * (content_len - 1);

        while (isspace(*content_cr_lf)) {
            *content_cr_lf = '\0';
            content_cr_lf -= sizeof(char);
        }

        /* Ignoring date headers as an temporary measure, if you try to set a
         * date header using the function smtp_set_header, it has a weird
         * behaviour, setting a different date, by now this headers will be
         * ignored and set automatically by libesmtp. */
        if (strncasecmp(p_header, "Date", 4) == 0) {
            continue;
        }

        /* When a Bcc header is found also is set as message recipient
         * but no header is added. */
        if (strncasecmp(p_header, "bcc", 3) == 0) {
            add_recipients(message, content);
            continue;
        }

        /* When a Cc header is found it is set as header and also as a
         * message recipient. */
        if (strncasecmp(p_header, "cc", 2) == 0) {
            smtp_set_header(message, p_header, NULL, content);
            add_recipients(message, content);
            continue;
        }

        /* When the library function can't set a header, then it will
         * be joined again in the buf as the example below. */
        if (!smtp_set_header(message, p_header, NULL, content) &&
            smtp_errno() == SMTP_ERR_INVAL) {
            /* Joining the header again, Title + Content */
            *colon =  ':';

            header_len = strlen(p_header);
            if (header_len + inval_headers_len +
                cr_lf_size >= inval_headers_size) {
                inval_headers_size = inval_headers_size + extra_len;
                if ((newptr = erealloc(inval_headers,
                                       inval_headers_size)) == NULL) {
                    efree(inval_headers);
                    inval_headers = NULL;
                    header_len = 0;
                    php_printf("Error: can't process header option: %s",
                               p_header);
                }
            inval_headers = newptr;
            }

            snprintf(inval_headers + inval_headers_len,
                     header_len + cr_lf_size, "%s%s", p_header, cr_lf);
            inval_headers_len = inval_headers_len + header_len
                              + cr_lf_size - 1;/*\0*/
        }
    }
    efree(mail_headers);
    *invalid_headers = inval_headers;
    return inval_headers_len;
}

static int
lf_to_crlf(char *string, size_t string_len, char **dest_string)
{
    char *line, *s_string, *new_string, *crlf, *newptr;
    int line_len, new_line_size, crlf_len;
    int new_string_len, extra_len, string_size;

    extra_len = (string_len / 78) + 20;
    string_size = string_len + extra_len + 1;
    new_string = emalloc(string_size);
    new_string_len = 0;
    s_string  = estrdup(string);

    while((line = strsep(&s_string, "\n")) != NULL) {

        line_len = strlen(line);
        if (line[line_len -1] == '\r') {
            crlf = "";
            crlf_len = 0;
        } else {
            crlf = "\r";
            crlf_len = 1;
        }

        new_line_size = line_len + crlf_len + 2;/*\n\0*/
        if (new_line_size + new_string_len >= string_size) {
            string_size = new_string_len + new_line_size + extra_len;
            if ((newptr = erealloc(new_string, string_size)) == NULL) {
                if (INI_INT("authmail.debug"))
                    php_printf("Error: can't process message line: %s", line);
                efree(new_string);
                new_string = NULL;
                new_line_size = 0;
            }
            new_string = newptr;
        }
        snprintf(new_string + new_string_len, new_line_size,
                 "%s%s\n", line, crlf);
        new_string_len = new_string_len + new_line_size - 1;/*\0*/
    }
    efree(s_string);
    *dest_string = new_string;
    return new_string_len + 1;
}

ZEND_DECLARE_MODULE_GLOBALS(authmail)

static function_entry authmail_functions[] = {
    PHP_FE(amail, NULL)
    {NULL, NULL, NULL}
};

zend_module_entry authmail_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_AUTHMAIL_EXTNAME,
    authmail_functions,
    PHP_MINIT(authmail),
    PHP_MSHUTDOWN(authmail),
    PHP_RINIT(authmail),
    NULL,
    PHP_MINFO(authmail),
#if ZEND_MODULE_API_NO >= 20010901
    PHP_AUTHMAIL_VERSION,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_AUTHMAIL
ZEND_GET_MODULE(authmail)
#endif

PHP_INI_BEGIN()

    /* Reading the authmail.address entry in .htaccess. */
    PHP_INI_ENTRY("authmail.address", "", PHP_INI_PERDIR, NULL)

    /* Reading the authmail.password entry in .htaccess. */
    PHP_INI_ENTRY("authmail.password", "", PHP_INI_PERDIR, NULL)

    /* Reading the authmail.smtp entry in .htaccess. */
    PHP_INI_ENTRY("authmail.smtp", "", PHP_INI_PERDIR, NULL)

    /* Reading the authmail.smtp_account entry in .htaccess. */
    PHP_INI_ENTRY("authmail.smtp_account", "", PHP_INI_PERDIR, NULL)

    /* Reading the authmail.default_port entry in .htaccess. */
    PHP_INI_ENTRY("authmail.port", "", PHP_INI_PERDIR, NULL)

    /* Reading the authmail.debug entry in .htaccess. */
    PHP_INI_ENTRY("authmail.debug", "0", PHP_INI_PERDIR, NULL)

    STD_PHP_INI_ENTRY("authmail.direction", "1", PHP_INI_PERDIR, OnUpdateBool,
                      direction, zend_authmail_globals, authmail_globals)
PHP_INI_END()

static void
php_authmail_init_globals(zend_authmail_globals *authmail_globals)
{
    authmail_globals->direction = 1;
}

PHP_RINIT_FUNCTION(authmail)
{
    zend_function *fe;

    /* Getting the amail function pointer. */
    if (zend_hash_find(EG(function_table), "amail", 6,
                       (void**)&fe) == FAILURE) {
        php_error_docref(NULL TSRMLS_CC, E_WARNING, "function amail not found");
        return FAILURE;
    }

    /* Deleting the original mail functions. */
    zend_hash_del(EG(function_table), "mail", 5);

    /* Adding the new mail function. */
    if (zend_hash_add(EG(function_table), "mail", 5, fe, sizeof(zend_function),
                      NULL) == FAILURE) {

        php_error_docref(NULL TSRMLS_CC, E_WARNING,
                         "Unable to add reference to new function name amail");
        zend_function_dtor(fe);
        return FAILURE;
    }
    AUTHMAIL_G(counter) = 0;

    return SUCCESS;
}

PHP_MINIT_FUNCTION(authmail)
{
    ZEND_INIT_MODULE_GLOBALS(authmail, php_authmail_init_globals, NULL);
    REGISTER_INI_ENTRIES();

    return SUCCESS;
}

PHP_MSHUTDOWN_FUNCTION(authmail)
{
    UNREGISTER_INI_ENTRIES();
    return SUCCESS;
}

PHP_MINFO_FUNCTION(authmail) {
    php_info_print_table_start( );
    php_info_print_table_header(2, "authmail support", "enabled");
    php_info_print_table_end( );
}

PHP_FUNCTION(amail)
{
    /* INI_ENTRIES */
    char *domain, *account, *password, *emailaddress, *ini_smtp, *ini_port, *at;
    size_t emailaddress_size, domain_size;

    /* Functions arguments */
    char *to, *subject, *message, *headers, *extra_cmd = "";

    int to_len, message_len, headers_len = 0;
    int subject_len, extra_cmd_len;

    /* Smtp variables */
    smtp_session_t am_session;
    smtp_message_t am_message;
    const smtp_status_t *status;

    /* Auth variables */
    auth_context_t am_authctx;
    authinfo_t ai;

    /* Message Variables */
    char *mail_message, *mail_subject, *extra_headers;
    char *mail_recipient, *message_str;
    int mail_message_size, mail_subject_size, message_str_size;
    int extra_headers_len;

    /* Monitor Variables */
    monitorinfo_t mi;
    int return_code, i;
    struct sigaction sa;

    /* NB.  libESMTP sets timeouts as it progresses through the protocol.
    In addition the remote server might close its socket on a timeout.
    Consequently libESMTP may sometimes try to write to a socket with
    no reader.  Ignore SIGPIPE, then the program doesn't get killed
    if/when this happens. */
    sa.sa_handler = SIG_IGN;
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction (SIGPIPE, &sa, NULL);

    mi.mi_debug = INI_INT("authmail.debug");

    if (PG(safe_mode) && (ZEND_NUM_ARGS() == 5)) {
        php_error_docref(NULL TSRMLS_CC, E_WARNING,
                         "SAFE MODE Restriction in effect. "
                         "The fifth parameter is disabled in SAFE MODE");
        RETURN_FALSE;
    }

    /* Reading function arguments. */
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sss|ss",  &to,
        &to_len, &subject, &subject_len, &message, &message_len,
        &headers, &headers_len, &extra_cmd, &extra_cmd_len) == FAILURE) {
        return;
    }

    if (to_len == 0) {
        if (mi.mi_debug)
            php_printf("Error: Empty recipient.");
        RETURN_FALSE
    }

    emailaddress = estrdup(INI_STR("authmail.address"));
    emailaddress_size = strlen(emailaddress);
    if (emailaddress_size == 0) {
        if (mi.mi_debug)
            notify("Error: No authmail.address entry has been found.");
        efree(emailaddress);
        RETURN_FALSE
    }

    password = estrdup(INI_STR("authmail.password"));
    if (strlen(password) == 0) {
        if (mi.mi_debug)
            notify("Error: No authmail.password entry has been found.");
        efree(emailaddress);
        efree(password);
        RETURN_FALSE
    }

    at = strchr(emailaddress, '@');
    if (at == NULL) {
        if (mi.mi_debug)
            notify("Error: No \"@\" in the authmail.address entry"
                       " has been found.");
        efree(emailaddress);
        efree(password);
        RETURN_FALSE
    }

    ini_port = INI_STR("authmail.port");
    ini_smtp = INI_STR("authmail.smtp");

    domain_size = strlen(at);
    domain  = emalloc(domain_size);
    snprintf(domain, domain_size, "%s", at + 1);

    if (strlen(INI_STR("authmail.smtp_account")) == 0) {
        emailaddress_size ++;
        account = emalloc(emailaddress_size);
        snprintf(account, emailaddress_size, "%s", emailaddress);
        account[emailaddress_size - domain_size - 1] = '=';
    } else {
        account = estrdup(INI_STR("authmail.smtp_account"));
    }

    /* Smtp session */
    am_session = smtp_create_session();

    if (set_server(am_session, ini_smtp, domain, ini_port) == 0) {
        efree(emailaddress);
        efree(password);
        efree(domain);
        efree(account);
        RETURN_FALSE
    }

    /* Message headers */
    if (subject_len > 0 && strncmp(&subject[subject_len - 2],
                                   "\r\n", 2) == 0) {
        mail_subject_size = subject_len - 1;
    } else {
        mail_subject_size = subject_len + 1;
    }

    mail_subject = emalloc(mail_subject_size);
    snprintf(mail_subject, mail_subject_size, "%s", subject);


    /* Auth session */
    auth_client_init();

    ai.ai_user   = account;
    ai.ai_pass   = password;
    ai.ai_domain = domain;

    am_authctx = auth_create_context();
    auth_set_mechanism_flags(am_authctx, AUTH_PLUGIN_PLAIN, 0);

    auth_set_interact_cb(am_authctx, authinteract, &ai);
    smtp_auth_set_context(am_session, am_authctx);

    /* Message */
    am_message = smtp_add_message(am_session);
    smtp_set_reverse_path(am_message, emailaddress);
    message_str_size = lf_to_crlf(message, message_len, &message_str);

    /* Headers */
    smtp_set_header(am_message, "To", NULL, to);
    smtp_set_header_option(am_message, "To", Hdr_OVERRIDE, 1);
    smtp_set_header_option(am_message, "Date", Hdr_OVERRIDE, 1);
    smtp_set_header_option(am_message, "Message-Id", Hdr_OVERRIDE, 1);

    if (headers_len == 0) {
        smtp_set_header(am_message, "From", NULL, emailaddress);
        smtp_set_header_option(am_message, "From", Hdr_OVERRIDE, 1);
        mail_message_size = message_str_size + 3;/*\r\n\0*/
        mail_message = emalloc(mail_message_size);
        snprintf(mail_message, mail_message_size, "\r\n%s", message_str);
    } else {
        extra_headers_len = set_headers(am_message, headers,
                                         headers_len, &extra_headers);
        mail_message_size  = extra_headers_len
                           + message_str_size + 3;/*\r\n\0*/
        mail_message = emalloc(mail_message_size);
        snprintf(mail_message, mail_message_size, "%s\r\n%s",
                 extra_headers, message_str);
        efree(extra_headers);
    }

    if (subject_len > 0) {
        smtp_set_header(am_message, "Subject", mail_subject);
        smtp_set_header_option(am_message, "Subject", Hdr_OVERRIDE, 1);
    }

    /* Mail message */
    smtp_set_message_str(am_message, mail_message);
    efree(message_str);

    /* Recipient */
    mail_recipient = estrdup(to);
    add_recipients(am_message, mail_recipient);

    /* Monitor */
    status = smtp_message_transfer_status(am_message);
    smtp_set_monitorcb(am_session, monitor_cb, &mi, 2);

    i = setjmp(mi.mi_jmpbuf);
    if (i == 0) {
        if (!smtp_start_session(am_session)) {
            if (mi.mi_debug) {
                char buf[128];
                php_printf("SMTP server problem %s.",
                           smtp_strerror(smtp_errno(), buf, sizeof buf));
            }
            longjmp(mi.mi_jmpbuf, 2);
        }
        if (mi.mi_debug)
            smtp_enumerate_recipients(am_message, print_recipient_status, NULL);
    }

    return_code = (status->code / 100 == 2);
    /* Free resources consumed by the program. */
    smtp_destroy_session(am_session);
    auth_destroy_context(am_authctx);
    auth_client_exit();
    efree(emailaddress);
    efree(domain);
    efree(account);
    efree(password);
    efree(mail_message);
    efree(mail_recipient);
    efree(mail_subject);

    RETURN_BOOL(return_code);
}
